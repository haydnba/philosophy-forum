astroid==2.1.0
coverage==4.5.2
Django==2.1.4
gunicorn==19.9.0
isort==4.3.4
lazy-object-proxy==1.3.1
mccabe==0.6.1
pylint==2.2.2
pylint-django==2.0.5
pylint-plugin-utils==0.4
# python-dotenv==0.10.1 (install locally only)
pytz==2018.7
six==1.12.0
whitenoise==4.1.2
wrapt==1.10.11
