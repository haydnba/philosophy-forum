from django.contrib import admin

from .models import Timetable, Week


# admin.site.register(Timetable)
# admin.site.register(Week)


@admin.register(Timetable)
class TimetableAdmin(admin.ModelAdmin):
    list_display = ('year', 'status', 'created', 'updated', 'author')
    list_filter = ('status',)
    # fieldsets = [
    #     (None, {'fields': ('year', 'status', 'author')}),
    #     ('Related', {'fields': ('weeks',)})
    # ]

@admin.register(Week)
class WeekAdmin(admin.ModelAdmin):
    list_display = ('year', 'week_of_year', 'monday', 'sunday')
    list_filter = ('year',)
