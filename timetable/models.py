from calendar import Calendar

from django.db import models
from django.db.utils import IntegrityError
from django.contrib.auth.models import User


class Timetable(models.Model):
    YEARS_CHOICES = [(i, i) for i in range(2010, 2050)]
    year = models.IntegerField(
        choices=YEARS_CHOICES,
        unique=True,
    )
    DRAFT = 'Draft'
    PUBLISHED = 'Published'
    STATUS_CHOICES = (
        (DRAFT, 'Draft'),
        (PUBLISHED, 'Published')
    )
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default=DRAFT
    )
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='timetables'
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    updated = models.DateTimeField(
        auto_now=True
    )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        year_template = Calendar().yeardatescalendar(self.year, width=12)[0]
        months_weeks = [week for month in year_template for week in month]
        unique_weeks_of_year = []
        for idx, week in enumerate(months_weeks):
            if months_weeks[idx - 1]:
                if months_weeks[idx - 1][0] != week[0]:
                    unique_weeks_of_year.append(week)
            else:
                unique_weeks_of_year.append(week)
        for idx, week in enumerate(unique_weeks_of_year, 1):
            try:
                week_record = Week.objects.create(
                    year=self.year,
                    week_of_year=idx,
                    monday=week[0],
                    sunday=week[6],
                    timetable=self
                )
                week_record.save()
            except IntegrityError as error:
                print(error)

    def __str__(self):
        return 'PF Timetable for {}'.format(self.year)

    class Meta:
        ordering = ('-year',)


class Week(models.Model):
    year = models.IntegerField()
    week_of_year = models.IntegerField(default=0)
    monday = models.DateField()
    sunday = models.DateField()
    timetable = models.ForeignKey(
        Timetable,
        on_delete=models.CASCADE,
        related_name='weeks'
    )

    def __str__(self):
        return 'Week {}, {}: Mon {} - Sun {}'.format(
            self.week_of_year, self.year, self.monday, self.sunday
        )

    class Meta:
        unique_together = (
            ('year', 'monday'),
            ('year', 'sunday'),
        )
        ordering = [
            '-year', 'week_of_year'
        ]

    # things for the manager?:

    def get_months(self):
        pass

    def get_quarter(self):
        pass

    def get_week_dates(self):
        pass
    
    def get_holidays(self):
        pass




# class TimetableManager(models.Manager):
#     def new_timetable(self, year):
#         timetable = super(TimetableManager, self).create(year=year)
#         timetable.save()
#         template = Calendar().yeardatescalendar(year)
#         for month in template:
#             for week in month:
#                 try:
#                     record = Week.objects.create(
#                         monday=week[0], sunday=week[6]
#                     )
#                     record.save()
#                 except IntegrityError:
#                     record = Week.objects.filter(
#                         monday=week[0], sunday=week[6]
#                     )
#                 record.timetables.add(timetable)
#         return timetable
